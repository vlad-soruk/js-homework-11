"use strict"

// Написати реалізацію кнопки "Показати пароль".
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, 
// які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - 
// інша іконка, саме вона повинна відображатися замість поточної.

// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)

// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)

let openedEye = document.body.querySelectorAll('.fa-eye');
let inputs = document.body.querySelectorAll('input');

for (let input of inputs) {
    if (input.classList.contains('enter-password')) {

        for (let eye of openedEye) {
            if (eye.id === "enter-pass-eye") {

                eye.addEventListener('click', function () {

                    if (eye.classList.contains('fa-eye-slash')) {
                        eye.classList.remove('fa-eye-slash');
                        eye.classList.add('fa-eye');   
        
                        input.type = "password";
                    }
        
                    else {
                        eye.classList.remove('fa-eye');
                        eye.classList.add('fa-eye-slash');
        
                        input.type = "text";
                }
                });

            }
        }
      
        
        
    }

    if (input.classList.contains('submit-password')) {

        for (let eye of openedEye) {
            if (eye.id === "submit-pass-eye") {

                eye.addEventListener('click', function () {

                    if (eye.classList.contains('fa-eye-slash')) {
                        eye.classList.remove('fa-eye-slash');
                        eye.classList.add('fa-eye');
                        
                        input.type = "password";
                    }
        
                    else {
                        eye.classList.remove('fa-eye');
                        eye.classList.add('fa-eye-slash');
        
                        input.type = "text";
                }
                });

            }
        }

    }
}


// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях

// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome

// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно
// ввести однакові значення

// Після натискання на кнопку сторінка не повинна перезавантажуватись

let submitButton = document.body.querySelector('form.password-form .btn');

let text = document.createElement('p');
inputs[1].after(text);

submitButton.addEventListener('click', function (event) {
    event.preventDefault();

    if (inputs[0].value != "" && inputs[1].value != "" && inputs[0].value === inputs[1].value){
        text.textContent = '';
        alert('You are welcome!')
    }
    else{
        text.textContent = "Потрібно ввести однакові значення";
        text.style.color = 'red';
    }
})